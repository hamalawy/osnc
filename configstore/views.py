from django.shortcuts import render
from configstore.models import DeviceConfig, DeviceLock
from rest_framework import viewsets
from rest_framework import permissions
from configstore.serializers import DeviceLockSerializer, DeviceConfigSerializer

# Create your views here.


class DeviceLockViewSet(viewsets.ModelViewSet):
    """
    This is a test description
    """
    description = 'This is a test for the description'
    queryset = DeviceLock.objects.all()
    serializer_class = DeviceLockSerializer
    permission_classes = [permissions.IsAuthenticated]


class DeviceConfigViewSet(viewsets.ModelViewSet):
    """
    This is a test description
    """
    description = 'This is a test for the description'
    queryset = DeviceConfig.objects.all()
    serializer_class = DeviceConfigSerializer
    permission_classes = [permissions.IsAuthenticated]