from django.db import models

# Create your models here.


class DeviceLock(models.Model):
    regex = models.CharField(max_length=255, blank=True, null=True)
    reason = models.CharField(max_length=255, blank=True, null=True)
    lock_id = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.regex


class DeviceConfig(models.Model):
    hostname = models.CharField(max_length=255, blank=True, null=True)
    config_id = models.CharField(max_length=255, blank=True, null=True)
    raw_config = models.TextField(blank=True, null=True)
    config_type = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.hostname

