from django.contrib.auth.models import User, Group
from configstore.models import DeviceConfig, DeviceLock
from rest_framework import serializers


class DeviceConfigSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = DeviceConfig
        fields = ['hostname', 'config_id', 'raw_config', 'config_type', 'created_at']


class DeviceLockSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = DeviceLock
        fields = ['regex', 'reason', 'lock_id', 'created_at']

