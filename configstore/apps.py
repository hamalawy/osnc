from django.apps import AppConfig


class ConfigstoreConfig(AppConfig):
    name = 'configstore'
