from django.contrib import admin
from configstore.models import *

# Register your models here.


@admin.register(DeviceLock)
class DeviceLockAdmin(admin.ModelAdmin):
    list_display = ('regex', 'reason', 'lock_id', 'created_at')
    search_fields = ('regex', 'reason', 'lock_id')

    def __str__(self):
        return 'Device' + self.regex


@admin.register(DeviceConfig)
class DeviceConfigAdmin(admin.ModelAdmin):
    list_display = ('hostname', 'config_id', 'config_type', 'created_at')
    search_fields = ('hostname', 'config_id', 'config_type')

    def __str__(self):
        return 'Device ' + self.hostname
