from django.apps import AppConfig


class NetdeployerConfig(AppConfig):
    name = 'netdeployer'
