from django.contrib import admin
from ncmanager.models import *
# Register your models here.

from ncmanager.models import *

# Register your models here.


@admin.register(AlertsModel)
class AlertsAdmin(admin.ModelAdmin):
    list_display = ('alertname', 'recepients', 'description', 'created_at')
    # search_fields = ('regex', 'reason', 'lock_id')
    

    def __str__(self):
        return 'System ' + self.alertname


@admin.register(UserNotificationModel)
class UserNotificationAdmin(admin.ModelAdmin):
    list_display = ('username', 'message', 'message_read', 'created_at')
    # search_fields = ('regex', 'reason', 'lock_id')

    def __str__(self):
        return 'System ' + self.message
