from django.apps import AppConfig


class NcmanagerConfig(AppConfig):
    name = 'ncmanager'
