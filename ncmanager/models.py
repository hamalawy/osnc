from django.db import models
from django.contrib.postgres.fields import ArrayField

# Create your models here.


class UserNotificationModel(models.Model):
    username = models.CharField(max_length=255, blank=True, null=True)
    message = models.CharField(max_length=255, blank=True, null=True)
    message_read = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.UserNotificationModel


class AlertsModel(models.Model):
    alertname = models.CharField(max_length=255, blank=True, null=True)
    recepients = models.CharField(max_length=255, blank=True, null=True)
    # recepients = ArrayField(
    #         models.CharField(max_length=255, blank=True, null=True),
    #     )
    description = models.TextField(editable=False)
    created_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.alertname
