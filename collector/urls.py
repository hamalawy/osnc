from django.contrib import admin
from django.urls import path, re_path, include, reverse
from django.conf.urls import url,include
import collector.views

app_name = 'collector'

urlpatterns = [
    url(r'^mychecks/$', collector.views.ChecksView, name='checks_view'),
    # url(r'^statistics/$', collector.views.StatisticsViews, name='collector_view')
]