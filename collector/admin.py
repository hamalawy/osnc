from django.contrib import admin
from .models import Device, Route, BgpRoute, Checks, Interface

# Register your models here.

admin.site.site_header = "Open Source Network Controller"
admin.site.site_title = "OSNC v1"
admin.site.index_title = "OSNC Manager"


@admin.register(Device)
class DeviceAdmin(admin.ModelAdmin):
    # inlines = [InLineInterface]
    list_display = ('hostname', 'type','version', 'created_at', 'updated_at')

    def __str__(self):
        return 'Device ' + self.hostname

# class InLineInterface(admin.TabularInline):
#     model=Interface

@admin.register(Interface)
class InterfaceAdmin(admin.ModelAdmin):
    list_display = ('hostname', 'interface', 'description', 'ip_address', 'mac', 'speed', 'status', 'enabled', 'created_at', 'updated_at')
    # readonly_fields = ('name', 'description', 'ip_address', 'status', 'enabled', 'created_at', 'updated_at', 'device')
    def __str__(self):
        return 'checkname: ' + self.checkname



@admin.register(Route)
class RouteAdmin(admin.ModelAdmin):
    list_display = ('device', 'routes', 'mroutes')

    def __str__(self):
        return 'Device ' + self.device


@admin.register(BgpRoute)
class BgpRouteAdmin(admin.ModelAdmin):
    list_display = ['route', 'nexthop', 'metric', 'localpref', 'weight', 'path', 'device']

    def __str__(self):
        return 'Route ' + self.route


@admin.register(Checks)
class ChecksAdmin(admin.ModelAdmin):
    list_display = ['checkname', 'active']
    # readonly_fields = ['active']

    def __str__(self):
        return 'checkname: ' + self.checkname
