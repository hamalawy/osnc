from django.test import TestCase, Client
from .models import Device, Checks, Interface
# Create your tests here.


class DeviceTestCase(TestCase):
    def setUp(self):
        """Create Dummy objects in database"""
        Device.objects.create(hostname="testr1", type="cisco", version="1.1")
        Device.objects.create(hostname="testr2", type="arista", version="1.2")

        Checks.objects.create(checkname='test_check1', active=True)
        Checks.objects.create(checkname='test_check2', active=False)

    def test_device_facts(self):
        """Check Database Test Cases and Filtering"""
        testr1 = Device.objects.get(hostname="testr1")
        testr2 = Device.objects.get(hostname="testr2")
        self.assertEqual(testr1.hostname, 'testr1')
        self.assertEqual(testr2.hostname, 'testr2')

    def test_checks(self):
        """Check Database Test Cases and Filtering"""
        check1 = Checks.objects.get(checkname="test_check1")
        check2 = Checks.objects.get(checkname="test_check2")

        self.assertEqual(check1.checkname, 'test_check1')
        self.assertEqual(check1.active, True)
        self.assertEqual(check2.checkname, 'test_check2')
        self.assertEqual(check2.active, False)
