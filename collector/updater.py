from apscheduler.schedulers.background import BackgroundScheduler
from collector.views import printer
def hello():
    print('hello')

def start():
    scheduler = BackgroundScheduler()
    scheduler.add_job(printer, 'interval', seconds=10)
    scheduler.start()

