# import netmiko
# import napalm
#
# config1 = """
# !
# upgrade fpd auto
# version 15.2
# service timestamps debug datetime msec
# service timestamps log datetime msec
# no service password-encryption
# !
# hostname r1
# !
# boot-start-marker
# boot-end-marker
# !
# !
# !
# no aaa new-model
# no ip icmp rate-limit unreachable
# !
# !
# !
# !
# !
# !
# no ip domain lookup
# ip domain name homelab
# ip cef
# login block-for 60 attempts 3 within 60
# login on-failure log
# login on-success log
# no ipv6 cef
# !
# multilink bundle-name authenticated
# !
# !
# !
# !
# !
# !
# !
# !
# !
# archive
#  log config
#   logging enable
#   notify syslog contenttype plaintext
#   hidekeys
# username hamalawy privilege 15 password 0 !@#123qwe
# !
# redundancy
# !
# !
# ip tcp synwait-time 5
# ip ssh version 2
# ip scp server enable
# !
# !
# !
# !
# !
# !
# !
# !
# !
# !
# interface FastEthernet0/0
#  ip address 192.168.0.32 255.255.255.0
#  duplex half
# !
# interface GigabitEthernet1/0
#  no ip address
#  shutdown
#  negotiation auto
# !
# interface GigabitEthernet2/0
#  no ip address
#  shutdown
#  negotiation auto
# !
# interface GigabitEthernet3/0
#  no ip address
#  shutdown
#  negotiation auto
# !
# interface GigabitEthernet4/0
#  no ip address
#  shutdown
#  negotiation auto
# !
# ip forward-protocol nd
# no ip http server
# no ip http secure-server
# !
# !
# !
# !
# logging host 192.168.1.1
# no cdp log mismatch duplex
# !
# !
# !
# control-plane
# !
# !
# !
# mgcp profile default
# !
# !
# !
# gatekeeper
#  shutdown
# !
# !
# line con 0
#  exec-timeout 0 0
#  privilege level 15
#  logging synchronous
#  stopbits 1
# line aux 0
#  exec-timeout 0 0
#  privilege level 15
#  logging synchronous
#  stopbits 1
# line vty 0 4
#  exec-timeout 0 0
#  logging synchronous
#  login local
#  transport input all
# !
# !
# end
# """
#
# config_list = config1.split('\n')
# print(config_list)
#
# device = netmiko.ConnectHandler(host="192.168.0.32", device_type="cisco_ios", username='hamalawy', password='!@#123qwe', verbose=True)
#
# output = device.send_command('ip scp server enable\n')
# print(output)
#
# # output = device.send_config_set(config_commands=config_list, delay_factor=10, cmd_verify=False,)
# output = device.enable_scp()
# print(output)
#
# output = device.send_config_set(config_commands=config_list, delay_factor=10, cmd_verify=False,)
# print(output)
#
# output = device.send_command('no ip scp server enable\n')
# print(output)


from netmiko.ssh_autodetect import SSHDetect
from netmiko.ssh_dispatcher import ConnectHandler
remote_device = {'device_type': 'autodetect',
                'host': '192.168.0.31',
                'username': 'hamalawy',
                'password': '!@#123qwe',
            }

guesser = SSHDetect(**remote_device)
best_match = guesser.autodetect()
print(best_match) # Name of the best device_type to use further
print(guesser.potential_matches) # Dictionary of the whole matching result
