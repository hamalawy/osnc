from nornir import InitNornir
nr = InitNornir(config_file="config.yaml")


def check_ping_drops(hostname, local_ip, remote_ip, count=100, packet_loss_threshold=0):
    """
    Ping checks that return the status and the packet loss
    """
    ping_result = hostname.ping(source=local_ip, destination=remote_ip, count=count, size=1300 ,)

    if ping_result['success']['packet_loss'] <= packet_loss_threshold:
        results = {'status': 'success', 'packet_loss': ping_result['success']['packet_loss']}
        status = 'Success'
        return status, results

    elif ping_result['success']['packet_loss'] > packet_loss_threshold:
        results = {'status': 'failed', 'packet_loss': ping_result['success']['packet_loss']}
        status = 'Failed'
        return status, results

    elif ping_result['error']:
        results = {'status': 'error', 'packet_loss': 100}
        status = 'Failed'
        return status, results


def check_interface_enabled(hostname, intf_list, expected_status):
    """
    Checks a list of interfaces against the expected status and returns True for success, False for failed
    """
    results = {'status': '', 'passed_interfaces': [], 'failed_interfaces': []}

    interfaces = hostname.get_interfaces()

    if expected_status == 'up':
        expected_status = True
    elif expected_status == 'down':
        expected_status = False

    for interface in interfaces:
        if interface in intf_list:
            if interfaces[interface]['is_enabled'] == expected_status:
                results['passed_interfaces'].append(interface)
            else:
                results['failed_interfaces'].append(interface)

    if not results['failed_interfaces']:
        results['status'] = 'Success'
        status = 'Success'
    else:
        results['status'] = 'Failed'
        status = 'Failed'

    return status, results


def check_interface_status(hostname, intf_list, expected_status):
    """
    Checks a list of interfaces against the expected status and returns True for success, False for failed
    """
    results = {'status': '', 'passed_interfaces': [], 'failed_interfaces': [] }

    interfaces = hostname.get_interfaces()

    if expected_status == 'up':
        expected_status = True
    elif expected_status == 'down':
        expected_status = False

    for interface in interfaces:
        if interface in intf_list:
            if interfaces[interface]['is_up'] == expected_status:
                results['passed_interfaces'].append(interface)
            else:
                results['failed_interfaces'].append(interface)

    if not results['failed_interfaces']:
        results['status'] = 'success'
        status = 'Success'
    else:
        results['status'] = 'failed'
        status = 'Failed'

    return status, results


# def check_interface_errors(hostname, intf_list):
#     """
#     Check if interface is counting errors
#     """
#     results = {'status': '', 'passed_interfaces': [], 'failed_interfaces': []}
#
#     interfaces = hostname.get_interfaces_counters()
#
#     for interface in interfaces:

devices_dict = {'192.168.0.32': 'eos',
                '192.168.0.90': 'ios',
                '192.168.0.83': 'ios',
                '192.168.0.108': 'ios',
                '192.168.0.42': 'ios',
    }
devices_list = ['192.168.0.32',
                '192.168.0.90',
                '192.168.0.83',
                '192.168.0.108',
                '192.168.0.42',
]

def check_alive(device):

    print(f'checking if {device.hostname} is alive')
    time.sleep(10)
    device.open()
    device.is_alive()
    print(device.is_alive()['is_alive'])
    device.close()
    print()

threads_list = []

# for d in devices_list:
#     driver = get_network_driver(devices_list[d])
#     # device = driver(d, 'hamalawy', '!@#123qwe', optional_args={'global_delay_factor': 2}, )
#     device = driver(d, 'hamalawy', '!@#123qwe')
#     T = Thread(target=check_alive(device), name=device.hostname)
#
#     threads_list.append(T)
#
# for i in threads_list:
#     i.start()
#
#     # check_alive(device)



