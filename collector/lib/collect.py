from napalm import get_network_driver
from collector.models import Device


from nornir import InitNornir
from nornir.core.task import Task, Result
from nornir_utils.plugins.functions import print_result, print_title
from nornir_netmiko.tasks import netmiko_send_command, netmiko_send_config
from nornir.core.filter import F
from pprint import pprint

nr = InitNornir(config_file="config.yaml", dry_run=True)

# import pdb; pdb.set_trace()


def get_facts():
    driver = get_network_driver('ios')
    device = driver('192.168.6.6', 'hamalawy', '!@#123qwe')
    device.open()
    """
    #TODO use context manager to avoid problems
    """
    facts = device.get_facts()
    Device.objects.update_or_create(hostname=facts['hostname'], type=facts['model'], version=facts['os_version'])
    device.close()


    # driver = get_network_driver('eos')
    # arista = driver('192.168.6.2', 'hamalawy', '!@#123qwe')
    # arista.open()
    # facts = arista.get_facts()
    # Device.objects.update_or_create(hostname=facts['hostname'], type=facts['model'], version=facts['os_version'])
    # arista.close()

    driver = get_network_driver('ios')
    device = driver('192.168.6.3', 'hamalawy', '!@#123qwe')
    device.open()
    facts = device.get_facts()
    Device.objects.update_or_create(hostname=facts['hostname'], type=facts['model'], version=facts['os_version'])
    device.close()

    driver = get_network_driver('ios')
    device = driver('192.168.6.4', 'hamalawy', '!@#123qwe')
    device.open()
    facts = device.get_facts()
    Device.objects.update_or_create(hostname=facts['hostname'], type=facts['model'], version=facts['os_version'])
    device.close()

    driver = get_network_driver('ios')
    device = driver('192.168.6.5', 'hamalawy', '!@#123qwe')
    device.open()
    facts = device.get_facts()
    Device.objects.update_or_create(hostname=facts['hostname'], type=facts['model'], version=facts['os_version'])
    device.close()

    # driver = get_network_driver('junos')
    # device = driver('192.168.6.7', 'hamalawy', '!@#123qwe')
    # device.open()
    # facts = device.get_facts()
    # Device.objects.update_or_create(hostname=facts['hostname'], type=facts['model'], version=facts['os_version'])
    # device.close()

