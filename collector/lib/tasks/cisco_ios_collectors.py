from nornir_netmiko.tasks import netmiko_send_command, netmiko_send_config
from nornir.core.task import Task, Result


def cisco_ios_collect_interfaces(task: Task) -> Result:
    """
    Parent Task for Cisco interfaces collector
    """
    task.run(
        name="RUNNING SHOW VERSIOM",
        task=cisco_ios_show_ver,
        # text="GETTING THE OUTPUT OF SHOW IP INTERFACES BRIEF",
    )

    task.run(
        name="RUNNING SHOW INTERFACES",
        task=cisco_ios_get_interface,
        # text="GETTING THE OUTPUT OF SHOW INTERFACES",
    )

    task.run(
        name="RUNNING GET CONFIGS",
        task=cisco_ios_get_configs,
        # text="GETTING THE OUTPUT OF SHOW INTERFACES",
    )



def cisco_ios_get_interface(task: Task) -> Result:
    output = task.run(netmiko_send_command,
                      command_string="show interface",
                      use_textfsm=True,
    )
    return Result(
        host=task.host,
        result=output,
    )


# def cisco_ios_get_interface_brief(task: Task) -> Result:
#     output = task.run(netmiko_send_command,
#                       command_string="show ip interface brief",
#                       use_textfsm=True,
#     )
#     return Result(
#         host=task.host,
#         result=output,
#     )


# def cisco_ios_get_interface_status(task: Task) -> Result:
#     output = task.run(netmiko_send_command,
#                       command_string="show interface status",
#                       use_textfsm=True,
#     )
#     return Result(
#         host=task.host,
#         result=output,
#     )





def cisco_ios_get_bgp_summary(task: Task) -> Result:
    output = task.run(netmiko_send_command,
                      command_string="show ip bgp summary",
                      use_textfsm=True,
    )
    return Result(
        host=task.host,
        result=output,
    )



def cisco_ios_get_facts(task: Task) -> Result:
    """
    Parent Task for Cisco interfaces collector
    """
    task.run(
        name="RUNNING SHOW VER",
        task=cisco_ios_show_ver,
        # text="GETTING THE OUTPUT OF SHOW INTERFACES",

    )


def cisco_ios_show_ver(task: Task) -> Result:
    output = task.run(netmiko_send_command,
                      command_string="show version",
                      use_textfsm=True,
    )
    return Result(
        host=task.host,
        result=output,
    )


def cisco_ios_get_configs(task: Task) -> Result:
    """
    Parent Task for Cisco Config Collector
    """
    task.run(
        name="RUNNING SHOW RUN",
        task=cisco_ios_get_show_run,
    )

    task.run(
        name="RUNNING SHOW STARTUP",
        task=cisco_ios_get_show_start,
    )




def cisco_ios_get_show_run(task: Task) -> Result:
    output = task.run(netmiko_send_command,
                      command_string="show run",
    )
    return Result(
        host=task.host,
        result=output,
    )

def cisco_ios_get_show_start(task: Task) -> Result:
    output = task.run(netmiko_send_command,
                      command_string="show start",
    )
    return Result(
        host=task.host,
        result=output,
    )