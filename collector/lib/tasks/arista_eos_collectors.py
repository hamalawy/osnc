from nornir_netmiko.tasks import netmiko_send_command, netmiko_send_config
from nornir.core.task import Task, Result

def arista_eos_collect_interfaces(task: Task) -> Result:
    """
    Parent task for Arista interface collector
    """
    task.run(
        name="Arista show ip int br",
        task=arista_eos_get_ip_interface_brief,
        # text="GETTING THE OUTPUT OF SHOW IP INTERFACES BRIEF",
    )


def arista_eos_get_ip_interface_brief(task: Task) -> Result:
    output = task.run(netmiko_send_command,
                        command_string="show ip int br",
                        use_textfsm=True,
            )
    return Result(
        host=task.host,
        result=output,
            )
