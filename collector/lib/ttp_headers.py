#!/usr/bin/env python
from netmiko import ConnectHandler
import os
from pprint import pprint

ciscor1 = {
    "device_type": "cisco_ios",
    "host": "192.168.6.3",
    "username": "hamalawy",
    "password": "!@#123qwe",
}

command = "show ip bgp | i \*>"

with ConnectHandler(**ciscor1) as net_connect:
    # Use TTP to retrieve structured data
    output = net_connect.send_command(
        command, use_ttp=True, ttp_template="templates/cisco/ios_show_ip_bgp2.ttp"
    )

print(output)


