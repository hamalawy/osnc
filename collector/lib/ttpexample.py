#!/usr/bin/env python
from netmiko import ConnectHandler
import os
from pprint import pprint

ciscor1 = {
    "device_type": "cisco_ios",
    "host": "192.168.0.30",
    "username": "hamalawy",
    "password": "!@#123qwe",
}

# write template to file
# ttp_raw_template = """
# interface {{ interface }}
#  description {{ description }}
# """

ttp_raw_template = """
{{ interface }} is {{ admin_status | ORPHRASE }}, line protocol is {{ line_status }}
  Hardware is iGbE, address is {{ mac-address }} (bia 0c72.6909.5a00)
  Internet address is {{ ip_address }}
  MTU {{ MTU }} bytes, BW 1000000 Kbit/sec, DLY 10 usec,
  5 minute input rate 1000 bits/sec, {{ input_pps }} packets/sec
  5 minute output rate 0 bits/sec, {{ output_ps }} packets/sec
     {{ total_packets_input }} packets input, 75779 bytes, 0 no buffer
     {{ input_errors }} input errors, 0 CRC, 0 frame, 0 overrun, 0 ignored
     {{ total_packets_output }} packets output, 81556 bytes, 0 underruns
     {{ output_errors }} output errors, 0 collisions, 1 interface resets
"""

# with open("show_run_interfaces.ttp", "w") as writer:
#     writer.write(ttp_raw_template)

command = "show interfaces"
with ConnectHandler(**ciscor1) as net_connect:
    # Use TTP to retrieve structured data
    output = net_connect.send_command(
        command, use_ttp=True, ttp_template="templates/cisco/ios_show_interfaces.ttp"
    )

import pdb; pdb.set_trace()
