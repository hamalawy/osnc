from nornir import InitNornir
from nornir.core.task import Task, Result
from nornir_utils.plugins.functions import print_result, print_title
from nornir_napalm.plugins.tasks import napalm_get

nr = InitNornir(config_file="config.yaml")


def hello_world(task: Task) -> Result:
    return Result(
        host=task.host,
        result=task.task
    )


device_types = {}


facts = nr.run(task=napalm_get, getters=['facts'])
for dev in facts:
    print(dev)
    model = facts[dev].result['facts']['model']
    manufacturer = facts[dev].result['facts']['vendor']
    device_types.update({model: manufacturer})


# interfaces = nr.run(task=napalm_get, getters=['interfaces'])
# for intf in interfaces:
#     model = interfaces[intf].result['facts']['model']
#     manufacturer = facts[dev].result['facts']['vendor']
#     device_types.update({model: manufacturer})


print(device_types)
print_title("Starting change")
print_result(facts)
