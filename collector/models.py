from django.db import models


# Create your models here.

class Device(models.Model):
    hostname = models.CharField(max_length=255)
    type = models.CharField(max_length=255, blank=True, null=True)
    version = models.CharField(max_length=255, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.hostname

    def body_preview(self):
        return self.body[:50]


class Route(models.Model):
    routes = models.TextField(blank=True)
    mroutes = models.TextField(blank=True)
    device = models.ForeignKey(Device, on_delete=models.CASCADE)

    def __str__(self):
        return self.routes


class BgpRoute(models.Model):
    route = models.CharField(max_length=17)
    nexthop = models.CharField(max_length=17)
    metric = models.IntegerField()
    localpref = models.IntegerField(null=True)
    weight = models.IntegerField()
    path = models.CharField(max_length=255)
    device = models.ForeignKey(Device, on_delete=models.CASCADE)

    def __str__(self):
        return self.route


class Checks(models.Model):
    checkname = models.CharField(max_length=255, blank=True, null=True, unique=True)
    active = models.BooleanField(default=False)

    def __str__(self):
        return self.checkname

    class Meta:
        verbose_name_plural = "Checks"


class Interface(models.Model):
    hostname = models.CharField(max_length=255, blank=True, null=True)
    interface = models.CharField(max_length=255, blank=True, null=True)
    description = models.CharField(max_length=255, blank=True, null=True)
    ip_address = models.GenericIPAddressField(blank=True, null=True)
    mac = models.CharField(max_length=255, blank=True, null=True)
    speed = models.CharField(max_length=255, blank=True, null=True)
    mtu = models.IntegerField(blank=True, null=True)
    status = models.BooleanField(default=False)
    enabled = models.BooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        unique_together = ('hostname', 'interface')
