import logging
from datetime import datetime
from nornirtest import run_nornir
from django.conf import settings
from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.triggers.cron import CronTrigger
from django.core.management.base import BaseCommand
from django_apscheduler.jobstores import DjangoJobStore
from django_apscheduler.models import DjangoJobExecution
from collector.models import Checks
from collector.lib.collect import get_facts
import django_apscheduler

logger = logging.getLogger(__name__)

# facts_status_results = Checks.objects.filter(checkname='facts').values()
# facts_status = facts_status_results[0]['active']


# def collect_device_facts():
#     """
#     Check if Collector fact is enabled in checks
#     """
#     facts_status_results = Checks.objects.filter(checkname='facts').values()
#     facts_status = facts_status_results[0]['active']

#     if facts_status:
#         print('job is active')
#         get_facts()
#         print('Device Data Collected')
#     else:
#         print('device facts check is deactivated')
#         pass


def delete_old_job_executions(max_age=604_800):
    """This job deletes all apscheduler job executions older than `max_age` from the database."""
    DjangoJobExecution.objects.delete_old_job_executions(max_age)


class Command(BaseCommand):
    help = "Runs apscheduler."

    def handle(self, *args, **options):
        scheduler = BlockingScheduler(timezone=settings.TIME_ZONE)
        scheduler.add_jobstore(DjangoJobStore(), "default",)

        # scheduler.add_job(
        #     collect_device_facts,
        #     trigger=CronTrigger(second="*/30"),  # Every 10 seconds
        #     id="collect_device_data",  # The `id` assigned to each job MUST be unique
        #     max_instances=1,
        #     replace_existing=True,
        # )

        scheduler.add_job(
            run_nornir,
            trigger=CronTrigger(minute="*/10"),  # Every 10 seconds
            id="run_nornir",  # The `id` assigned to each job MUST be unique
            max_instances=1,
            replace_existing=True,
            next_run_time=datetime.now(),
        )

        scheduler.add_job(
            delete_old_job_executions,
            trigger=CronTrigger(
                day_of_week="mon", hour="00", minute="00"
            ),  # Midnight on Monday, before start of the next work week.
            id="delete_old_job_executions",
            max_instances=1,
            replace_existing=True,
        )
        logger.info(
            "Added weekly job: 'delete_old_job_executions'."
        )

        while True:


            try:
                logger.info("Starting scheduler...")
                print("starting Scheduler")
                scheduler.start()

            except KeyboardInterrupt:
                logger.info("Stopping scheduler...")
                scheduler.shutdown()
                logger.info("Scheduler shut down successfully!")