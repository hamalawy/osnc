from django.contrib.auth.models import User, Group
from collector.models import Device, Route, Checks
from rest_framework import serializers


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ['url', 'username', 'email', 'groups']


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ['url', 'name']


class DeviceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Device
        fields = ['hostname', 'type', 'version', 'created_at', 'updated_at']


class RouteSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Route
        fields = ['routes', 'mroutes', 'device']


class ChecksSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Checks
        fields = ['id', 'checkname', 'active']

