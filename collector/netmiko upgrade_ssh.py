from netmiko import ConnectHandler
import time
import logging

logging.basicConfig(filename='upgrader.log', level=logging.DEBUG)
logger = logging.getLogger("netmiko")

device = {
    'host': '192.168.0.30',
    'port': 22,
    'username': 'hamalawy',
    'password': '!@#123qwe',
    'device_type': 'cisco_ios'
}

conn = ConnectHandler(**device)
prompt = conn.find_prompt(delay_factor=3)
print(prompt)

if conn.is_alive():
    print("device is alive alive")
else:
    print('dead')

save_memory = conn.save_config()
print(save_memory)

try:
    output = conn.send_command(command_string="reload reason os upgrade", cmd_verify=False,  expect_string='Proceed with reload')
except:
    print('reload rason')



print('sleeping for 60 seconds for device to reboot')
time.sleep(60)


while conn.is_alive() is False:
    time.sleep(30)
    print('device still booting')
else:
    print(conn.is_alive())



while conn.find_prompt() != 'cisco-r1#':
    time.sleep(20)
    if conn.find_prompt() == 'cisco-r1#':
        print('prompt found')
        break


