from django.shortcuts import render, redirect
from django.contrib.auth.models import User, Group
from collector.models import Device, Route, Checks
from rest_framework import viewsets
from rest_framework import permissions
from collector.serializers import UserSerializer, GroupSerializer, DeviceSerializer, RouteSerializer, ChecksSerializer
from django.urls import reverse

# Create your views here.


# class UserViewSet(viewsets.ModelViewSet):
#     """
#     API endpoint that allows users to be viewed or edited.
#     """
#     queryset = User.objects.all().order_by('-date_joined')
#     serializer_class = UserSerializer
#     permission_classes = [permissions.IsAuthenticated]
#
#
# class GroupViewSet(viewsets.ModelViewSet):
#     """
#     API endpoint that allows groups to be viewed or edited.
#     """
#     queryset = Group.objects.all()
#     serializer_class = GroupSerializer
#     permission_classes = [permissions.IsAuthenticated]

class DeviceViewSet(viewsets.ModelViewSet):
    """
    This is a test description
    """
    description = 'This is a test for the description'
    queryset = Device.objects.all() #FIXME Get specific results instead of all, 
    serializer_class = DeviceSerializer
    permission_classes = [permissions.IsAuthenticated]


class RouteViewSet(viewsets.ModelViewSet):
    """
    This is a test description
    """
    description = 'This is a test for the description'
    queryset = Route.objects.all()
    serializer_class = RouteSerializer
    permission_classes = [permissions.IsAuthenticated]


class ChecksViewSet(viewsets.ModelViewSet):
    """
    This is a test description
    """
    description = 'This is a test for the description'
    queryset = Checks.objects.all()
    serializer_class = ChecksSerializer
    permission_classes = [permissions.IsAuthenticated]




def start(request):
    """
    Start page with a documentation.
    """
    print("writing to database")
    # Users.objects.using("users").all()
    # Users.objects.update_or_create(email='hamalawy@gmail.com')
    print("hello")

    print("wrote to database")
    return render(request, "django_sb_admin/start.html",
                  {"nav_active":"start"})


def dashboard(request):
    """Dashboard page.
    """
    return render(request, "django_sb_admin/sb_admin_dashboard.html",
                  {"nav_active":"dashboard"})


def charts(request):
    """Charts page.
    """
    # return render(request,"django_sb_admin/sb_admin_charts.html", {"prefixes_per_neighbor": "prefixes_per_neighbor"} )
    return render(request, "django_sb_admin/sb_admin_charts.html",
                  {"nav_active":"charts"})

# def tables(request):
#     """Tables page.
#     """
#     print(request)
#     context = {}
#     context["prefixes_per_neighbor"] = prefixes_per_neighbor
#     return render(request, "django_sb_admin/ext_peerings.html", context)

def forms(request):
    """Forms page.
    """
    return render(request, "django_sb_admin/sb_admin_forms.html",
                  {"nav_active":"forms"})


def bootstrap_elements(request):
    """Bootstrap elements page.
    """
    return render(request, "django_sb_admin/sb_admin_bootstrap_elements.html",
                  {"nav_active":"bootstrap_elements"})


def bootstrap_grid(request):
    """Bootstrap grid page.
    """
    return render(request, "django_sb_admin/sb_admin_bootstrap_grid.html",
                  {"nav_active":"bootstrap_grid"})


def dropdown(request):
    """Dropdown  page.
    """
    return render(request, "django_sb_admin/sb_admin_dropdown.html",
                  {"nav_active":"dropdown"})


def rtl_dashboard(request):
    """RTL Dashboard page.
    """
    return render(request, "django_sb_admin/sb_admin_rtl_dashboard.html",
                  {"nav_active":"rtl_dashboard"})


def blank(request):
    """Blank page.
    """
    print(request.GET)
    print(request.POST)
    return render(request, "django_sb_admin/sb_admin_blank.html",
                   {"nav_active":"blank"})


def tables(request):
    """Tables page.
    """
    return render(request, "django_sb_admin/sb_admin_tables.html",
                  {"nav_active":"tables"})


from django.views.generic.edit import FormView
from django.http import HttpResponse
from .forms import NewArticeForm, ChecksForm


def valid_form(request):
    return HttpResponse("Form submitted successfully!")


class NewArticleView(FormView):
    template_name = "blog/new.html"
    form_class = NewArticeForm
    success_url = '/dashboard/'

    def form_valid(self, form):
        form.valid_submission_callback()
        return super().form_valid(NewArticeForm)
            #super(NewArticleForm, self).form_valid(form)


def ChecksView(request):
    form = ChecksForm()
    if request.method == "POST":
        form = ChecksForm(request.POST)
        if form.is_valid():
            print(form.data)
            Checks.objects.update_or_create(**form.cleaned_data)
        else:
            print(form.errors)
    context = {
        'form': form,
        'messages': ["Database entries will be updated", "Please Check admin panels for editing checks"]
    }
    # return redirect('collector:ChecksView')
    print('inside checksview')
    return render(request, "django_sb_admin/sb_admin_blank.html", context)

def myprinter():
    print('hello')