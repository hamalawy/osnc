# Generated by Django 3.1.4 on 2021-02-01 01:25

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('collector', '0006_bgproute'),
    ]

    operations = [
        migrations.AlterField(
            model_name='bgproute',
            name='nexthop',
            field=models.CharField(max_length=17),
        ),
        migrations.AlterField(
            model_name='bgproute',
            name='routes',
            field=models.CharField(max_length=17),
        ),
    ]
