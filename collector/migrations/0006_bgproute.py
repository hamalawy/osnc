# Generated by Django 3.1.4 on 2021-01-31 22:11

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('collector', '0005_auto_20210131_1606'),
    ]

    operations = [
        migrations.CreateModel(
            name='BgpRoute',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('routes', models.TextField(blank=True)),
                ('nexthop', models.TextField(blank=True)),
                ('metric', models.IntegerField()),
                ('localpref', models.IntegerField(null=True)),
                ('weight', models.IntegerField()),
                ('path', models.CharField(max_length=255)),
                ('device', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='collector.device')),
            ],
        ),
    ]
