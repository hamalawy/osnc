from django import forms


class NewArticeForm(forms.Form):
    title = forms.CharField(label="Title", max_length=255)

    def valid_submission_callback(self):
        # send an email or other backend call back
        pass


class ChecksForm(forms.Form):
    checkname = forms.CharField(
        label="Check Name",
        required=True,
        help_text="Please refer to the documentation for check types",
        widget=forms.TextInput(
            attrs={
                "placeholder": "route_check",
                # "onclick": "window.location.href='/admin/'",
            },
        )
    )
    active = forms.BooleanField(required=False)

    def valid_submission_callback(self):
        # send an email or other backend call back
        print('this is something happening')
        pass
