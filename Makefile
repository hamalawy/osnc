update_requirements:
	pip freeze > requirements.txt
	conda env export > environment.yml