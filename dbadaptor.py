from collector.models import Interface, Device
from configstore.models import DeviceConfig
from datetime import datetime
class CiscoIosParser:

    @staticmethod
    def show_ver_parser(object):
        for devices in object.keys(): 
            object[devices].result.result

            for dev in object[devices].result.result:
                device_info = {
                # 'hostname': dev['hostname'],
                'type': dev['hardware'],
                'version': dev['version'],
                }
                Device.objects.update_or_create(hostname=dev['hostname'], **device_info)
        pass


    @staticmethod
    def show_interface_parser(object):
        for device in object.keys():
            import pdb; pdb.set_trace()
            for interface in object[device].result.result:
                if interface['link_status'] == 'up':
                    status = True
                else:
                    status = False
                
                if interface['protocol_status'] == 'up':
                    enabled = True
                else:
                    enabled = False

                interface_info = {
                'mtu': interface['mtu'],
                'description': interface['description'],
                'mac': interface['address'],
                'speed': interface['speed'],
                'status': status,
                'enabled': enabled
                }

                Interface.objects.update_or_create(
                    hostname=device,
                    interface=interface['interface'],
                    defaults=interface_info
                    )
        pass

    @staticmethod
    def running_config_save(object):
        for device in object.keys():
            now = datetime.now().strftime("%y%m%d%H%M%S")
            config = object[device][0].result[0].result
            config_id = f'{device}_{now}'
            update_params = {
                'raw_config': config,
                'config_id':config_id,
                }
            DeviceConfig.objects.update_or_create(
                hostname=device,
                config_type='running',
                defaults= update_params,
            )
    
    @staticmethod
    def startup_config_save(object):
        for device in object.keys():
            now = datetime.now().strftime("%y%m%d%H%M%S")
            config = object[device][0].result[0].result
            config_id = f'{device}_{now}'
            update_params = {
                'raw_config': config,
                'config_id':config_id,
                }
            DeviceConfig.objects.update_or_create(
                hostname=device,
                config_type='startup',
                defaults= update_params,
            )
 

    



