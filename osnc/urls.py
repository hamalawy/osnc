"""osnc URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path, include
from django.conf.urls import url,include
import collector.views
import configstore.views
import collector.urls
from rest_framework import permissions, routers
from drf_yasg2.views import get_schema_view
from drf_yasg2 import openapi



schema_view = get_schema_view(
      openapi.Info(
         title="OSNC API",
         default_version='v1',
         description="Open Source Network Controller",
         terms_of_service="https://www.google.com/policies/terms/",
         contact=openapi.Contact(email="support@osnc.org"),
         license=openapi.License(name="BSD License"),
      ),
      public=True,
      permission_classes=(permissions.AllowAny,),
   )


router = routers.DefaultRouter()
# router.register(r'users', views.UserViewSet)
# router.register(r'groups', views.GroupViewSet)
router.register(r'device', collector.views.DeviceViewSet)
router.register(r'route', collector.views.RouteViewSet)
router.register(r'checks', collector.views.ChecksViewSet)
router.register(r'devicelock', configstore.views.DeviceLockViewSet)
router.register(r'deviceconfig', configstore.views.DeviceConfigViewSet)

urlpatterns = [
    url(r'^$', collector.views.start, name='start'),
    url(r'^new$', collector.views.NewArticleView.as_view(), name="new"),
    # path('collector', include(collector.urls)),
    path('admin/', admin.site.urls),
    path('collector/', include("collector.urls")),
    path('', include(router.urls)),
    path('api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    path('swagger/', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    path('redoc/', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
    url(r'^dashboard/$', collector.views.dashboard, name='sb_admin_dashboard'),
    url(r'^charts/$', collector.views.charts, name='sb_admin_charts'),
    url(r'^tables/$', collector.views.tables, name='sb_admin_tables'),
    url(r'^forms/$', collector.views.forms, name='sb_admin_forms'),
    url(r'^bootstrap-elements/$', collector.views.bootstrap_elements, name='sb_admin_bootstrap_elements'),
    url(r'^bootstrap-grid/$', collector.views.bootstrap_grid, name='sb_admin_bootstrap_grid'),
    url(r'^rtl-dashboard/$', collector.views.rtl_dashboard, name='sb_admin_rtl_dashboard'),
    url(r'^blank/$', collector.views.blank, name='sb_admin_blank'),

    ]
