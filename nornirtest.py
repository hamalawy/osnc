from collector.models import Interface
import sys
import os
import dbadaptor
from nornir_netmiko.tasks.netmiko_send_command import netmiko_send_command
print(os.getcwd())
print(sys.executable)
from collector.lib.tasks.cisco_ios_collectors import cisco_ios_get_interface, cisco_ios_show_ver
from nornir import InitNornir
from nornir.core.task import Task, Result
from nornir_utils.plugins.functions import print_result, print_title
from nornir.core.filter import F
import nornir.core.inventory
from pprint import pprint
from collector.lib.tasks.cisco_ios_collectors import cisco_ios_get_interface, cisco_ios_get_show_run, cisco_ios_get_show_start
from collector.lib.tasks.arista_eos_collectors import arista_eos_collect_interfaces
from nornir.core.plugins.inventory import TransformFunctionRegister
import getpass


# import pdb; pdb.set_trace()
x = nornir.core.inventory.ConnectionOptions(username='hamalwy', password='!@#123qwe')

def populate_creds(host, **kwargs):
    host.username = 'hamalawy'
    host.password = '!@#123qwe'


# You give it a name to register it as and a function here
TransformFunctionRegister.register("my_transfer_function", populate_creds)

def run_nornir():
    nr = InitNornir(
        logging={
            "log_file": "mylogs.log",
            "level": "DEBUG",
            "enabled": False
        },
        inventory = {
            "plugin" : "SimpleInventory",
            "transform_function": "my_transfer_function",
            "options": {
                "host_file": "/home/hamalawy/github/osnc/collector/lib/inventory/hosts.yaml",
                "group_file": '/home/hamalawy/github/osnc/collector/lib/inventory/groups.yaml'
            }
        }
    )

    nr.runner.num_workers = 10

    platform_list = ['ios']

    for platform in platform_list:
        if platform == 'ios':
            cisco = nr.filter(F(platform=platform))
            cisco_result = cisco.run(name='Running All Cisco Tasks', task=cisco_ios_get_interface)
            cisco_ver_result  = cisco.run(name='Getting Cisco Facts', task=cisco_ios_show_ver) 
            cisco_sh_run_result = cisco.run(name='Getting Running Configs', task=cisco_ios_get_show_run)
            cisco_sh_start_result = cisco.run(name='Getting Running Configs', task=cisco_ios_get_show_start)
            # cisco_result = task.run(netmiko_send_command, command_string='show interface', use_textfsm=True)
            print_result(cisco_result)
            cisco.close_connections()
        # elif platform == 'eos':
        #     arista = nr.filter(F(platform=platform))
        #     arista_result = arista.run(name='Running All Arista Tasks', task=arista_collect_interfaces)
        #     print_result(arista_result)


    # dbadaptor.CiscoIosParser.show_interface_parser(cisco_result)
    # dbadaptor.CiscoIosParser.show_ver_parser(cisco_ver_result)
    # dbadaptor.CiscoIosParser.running_config_save(cisco_sh_run_result)
    # dbadaptor.CiscoIosParser.startup_config_save(cisco_sh_start_result)

    # for name, host in nr.inventory.hosts.items():
    #     print(f"{name}.username: {host.password}")

