import argparse
import sys
import os
import re

from nornir_netmiko.tasks.netmiko_send_command import netmiko_send_command
print(os.getcwd())
print(sys.executable)
from nornir import InitNornir
from nornir.core.task import Task, Result
from nornir_utils.plugins.functions import print_result, print_title
from collector.lib.tasks.cisco_ios_collectors import cisco_ios_get_interface, cisco_ios_get_bgp_summary
from nornir.core.filter import F
import nornir.core.inventory
from pprint import pprint
from nornir.core.plugins.inventory import TransformFunctionRegister
import getpass
from tabulate import tabulate

# import pdb; pdb.set_trace()
x = nornir.core.inventory.ConnectionOptions(username='hamalwy', password='!@#123qwe')

def populate_creds(host, **kwargs):
    host.username = 'hamalawy'
    host.password = '!@#123qwe'


# You give it a name to register it as and a function here
TransformFunctionRegister.register("my_transfer_function", populate_creds)

parser = argparse.ArgumentParser()
parser.add_argument("-g", "--get", help="Get action retrieves config")
parser.add_argument("--hostname", required=True ,help="one more host to perform action on [full names separated by comma] ")
parser.add_argument("--ignored" ,help="one more host to perform action on [full names separated by comma] ")
parser.add_argument("--verbose",action='store_true', help=" print out the nornir operations output")

args = parser.parse_args()

hostname = args.hostname
options = {
    'ignored': args.ignored
}
verbose = args.verbose

def get_stats(hostname, **kwargs):
    print('running nornir')
    nr = InitNornir(
        logging={
            "log_file": "mylogs.log",
            "level": "DEBUG",
            "enabled": True
        },
        inventory = {
            "plugin" : "SimpleInventory",
            "transform_function": "my_transfer_function",
            "options": {
                "host_file": "/home/hamalawy/github/osnc/collector/lib/inventory/hosts.yaml",
                "group_file": '/home/hamalawy/github/osnc/collector/lib/inventory/groups.yaml'
            }
        }
    )
    nr.runner.num_workers = 10
    hostlist = list(nr.inventory.hosts.keys())
    r = re.compile(hostname)
    filtered_hostlist = list(filter(r.match, hostlist))
    print(filtered_hostlist)
    platform_list = ['ios']

    cisco_list = []
    arista_list = []


    for target in filtered_hostlist:
        if nr.inventory.hosts[target].platform == 'ios':
            cisco_list.append(target)
        elif nr.inventory.hosts[target].platform == 'eos':
            arista_list.append(target)
    
    
    cisco = nr.filter(F(name__in=cisco_list) & ~F(name=kwargs['ignored']))
    arista = nr.filter(F(name__in=arista_list) & ~F(name=kwargs['ignored']))



    for platform in platform_list:
        if platform == 'ios':
            if kwargs['ignored']:
                cisco = nr.filter(F(name__in=filtered_hostlist) & ~F(name=kwargs['ignored']))
            else:
                cisco = nr.filter(F(name__in=filtered_hostlist))
            interfaces_result = cisco.run(name='Running All Cisco Tasks', task=cisco_ios_get_interface)
            bgp_result = cisco.run(name='Running All Cisco Tasks', task=cisco_ios_get_bgp_summary)

            # cisco_result = task.run(netmiko_send_command, command_string='show interface', use_textfsm=True)
            print_result(interfaces_result) if verbose else ""
            cisco.close_connections()
            
    for host in interfaces_result:
        if interfaces_result[host].failed:
            print(f"Failed to get results for {host}")
            continue
        else:
            print(host+"\n\n=============")
            print(tabulate(interfaces_result[host][1].result,headers="keys"))
            print("\n")
            print(tabulate(bgp_result[host][1].result,headers="keys"))
            print("\n\n")
    
get_stats(hostname, **options)
