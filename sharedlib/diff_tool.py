import difflib
import os
import hashlib
os.chdir('/Users/hamalawy/gitlab/osnc/misc')


def create_diff(textleft, textright):
    """
    Compared two strings and return two pane html output
    Strings are converted to lists since it only supports lists
    """
    if type(textleft) is str and type(textright) is str:
        testleft = textleft.split('\n')
        testright = textright.split('\n')
    elif type(textleft) is list and type(textright) is list:
        pass
    else:
        raise Exception("Error: Wrong inputs")

    delta = difflib.HtmlDiff(wrapcolumn=88).make_file(textleft, textright)
    filename = hashlib.sha256((str(frozenset(textleft+textright))).encode('utf-8')).hexdigest()
    filename = filename[54:]
    file = open('/tmp/{}.html'.format(filename), 'w')
    print("Created Diff file /tmp/{}.html".format(filename))
    file.write(delta)
    file.close()
    return delta


file1 = open('f1', 'r')
file2 = open('f2', 'r')
f1l = file1.readlines()
f2l = file2.readlines()

dhtml = create_diff(f1l, f2l)

