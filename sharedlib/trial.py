x = """Building configuration...

Current configuration : 27105 bytes
!
! Last configuration change at 18:53:22 UTC Tue Nov 5 2019 by hamalawy
! NVRAM config last updated at 18:53:23 UTC Tue Nov 5 2019 by hamalawy
!
version 15.6
service timestamps debug datetime msec
service timestamps log datetime msec
service password-encryption
service unsupported-transceiver
no platform punt-keepalive disable-kernel-core
platform hardware throughput level 36000000
!
hostname TLNX-YYZ-TR1-CR01
!
boot-start-marker
boot system flash bootflash:asr1002x-universalk9.03.17.01.S.156-1.S1-std.SPA.bin
boot system flash 
boot-end-marker
!
!
vrf definition Mgmt-intf
 rd 64.16.255.223:1
 route-target export 63440:1
 route-target import 63440:1
 !
 address-family ipv4
 exit-address-family
 !
 address-family ipv6
 exit-address-family
!
vrf definition TELNYX
 rd 64.16.255.223:1001
 route-target export 63440:1001
 route-target import 63440:1001
 !
 address-family ipv4
  route-target export 63440:1001
  route-target import 63440:1001
 exit-address-family
 !
 address-family ipv6
 exit-address-family
!
logging discriminator DISCRIMI mnemonics drops ntp_receive 
logging count
logging userinfo
logging buffered discriminator DISCRIMI
!
aaa new-model
!
!
aaa group server tacacs+ TELNYX
 server 10.1.129.30
 server-private 10.1.129.30 key 7 040F25031B361C5C02581116111B0D17393C2B3A37
 ip vrf forwarding TELNYX
 ip tacacs source-interface Loopback1001
!
aaa authentication login default group TELNYX local
aaa authentication login CONSOLE none
aaa authentication enable default group TELNYX enable
aaa authorization config-commands
aaa authorization exec default group TELNYX local if-authenticated 
aaa authorization commands 0 default group TELNYX local if-authenticated 
aaa authorization commands 1 default group TELNYX local if-authenticated 
aaa authorization commands 7 default group TELNYX local if-authenticated 
aaa authorization commands 15 default group TELNYX local if-authenticated 
aaa accounting commands 0 default start-stop group TELNYX
aaa accounting commands 1 default start-stop group TELNYX
aaa accounting commands 7 default start-stop group TELNYX
aaa accounting commands 15 default start-stop group TELNYX
aaa accounting network 0 start-stop group TELNYX
aaa accounting network 15 start-stop group TELNYX
aaa accounting connection 0 start-stop group TELNYX
aaa accounting connection 15 start-stop group TELNYX
!
!
!
!
!
aaa session-id common
!
!
!
!
!
!
!
!
!



ip domain name telnyx.com
!
!
!
ipv6 unicast-routing
!
!
!
!
!
!
!
subscriber templating
!
!
!
sampler NETFLOW.SAMPLER
 description SAMPLE at 1:1000
 mode random 1 out-of 1000
!
multilink bundle-name authenticated
!
!
!
!
!
!
!
!
!
license udi pid ASR1002-X sn JAE201702MA
license accept end user agreement
license boot level adventerprise
archive
 log config
  logging enable
  notify syslog contenttype plaintext
 path bootflash:
 maximum 14
 rollback filter adaptive
 rollback retry timeout 10
 write-memory
!
spanning-tree extend system-id
!
username tena privilege 15 secret 5 $1$c4mO$MCnf9xWXkLTzyXDMUfgBc0
username network privilege 15 secret 5 $1$D3vL$JF2WstIP41tM/qVIYH.xY0
username tena-pipeline privilege 15 secret 5 $1$HqjP$oNXUQ63Cqxc8i30bXp2U00
!
redundancy
 mode none
 application redundancy
  group 1
   name RG1
   preempt
   priority 120
   control Port-channel1.3201 protocol 1
   data Port-channel1.3201
   asymmetric-routing interface Port-channel1.3201
   asymmetric-routing always-divert enable
   track 101 decrement 50
  protocol 1
   name RG1
  dampening 100
bfd-template single-hop BFD-500X-5X-INT-STD
 interval min-tx 500 min-rx 500 multiplier 5
!
bfd-template single-hop BFD-750X-3X-DAMP-EXT-STD
 interval min-tx 750 min-rx 750 multiplier 3
!
bfd-template single-hop BFD-750X-3X-INT-STD
 interval min-tx 750 min-rx 750 multiplier 3
!
bfd-template single-hop BFD-850X-3X-GTT-STD
 interval min-tx 850 min-rx 850 multiplier 3
!
!
!
!
!
!
!
track 101 interface Port-channel1.3201 line-protocol
!
track 201 stub-object
 delay up 10
!
ip tcp path-mtu-discovery
lldp run
!
! 
!
!
!
!
!
!
!
!
!
!
!
!
! 
! 
! 
! 
! 
! 
!
!
interface Loopback0
 description LOOPBACK_CORE_TLNX-YYZ-TR1-CR01
 ip address 64.16.255.223 255.255.255.255
!
interface Loopback1
 description LOOPBACK_NTP_TLNX-YYZ-TR1-CR01
 ip address 64.16.255.1 255.255.255.255
!
interface Loopback1001
 description LOOPBACK_MGMT_TLNX-YYZ-TR1-CR01
 vrf forwarding TELNYX
 ip address 172.18.0.36 255.255.255.255
!
interface Port-channel1
 description LAG_TLNX-YYZ-TR1-AG01_AE3
 mtu 9000
 no ip address
 carrier-delay msec 0
 lacp fast-switchover
!
interface Port-channel1.10
 description LINK:DESCRIPTION:FORMAT:WILL:GO:HERE:TYPE-L3-VRF-LOOPBACK
 encapsulation dot1Q 10
 vrf forwarding TELNYX
 ip address 172.18.16.2 255.255.255.248
 standby 10 ip 172.18.16.1
 standby 10 priority 120
 standby 10 preempt
!
interface Port-channel1.101
 description CLD_TR1_IBM_VL101_L3-CR_:0130-1723-A:
 encapsulation dot1Q 101
 vrf forwarding TELNYX
 ip address 10.254.1.254 255.255.255.252
!
interface Port-channel1.1019
 description LINK:TLNX-ORD-CH1-CR01-CHANGING-FROM-70-TO-10070-ISIS-VALUE
 bandwidth 1000000
 encapsulation dot1Q 1019
 ip address 64.16.254.198 255.255.255.254
 ip mtu 8000
 ip router isis TELNYX-BACKBONE
 bfd template BFD-850X-3X-GTT-STD
 clns mtu 1500
 isis network point-to-point 
 isis metric 10070
!
interface Port-channel1.1021
 description LINK:TLNX-YVR-VAN2-CR01
 bandwidth 1000000
 encapsulation dot1Q 1021
 ip address 64.16.254.202 255.255.255.254
 ip mtu 8000
 ip router isis TELNYX-BACKBONE
 bfd template BFD-850X-3X-GTT-STD
 clns mtu 1500
 isis network point-to-point 
 isis metric 336
!
interface Port-channel1.3201
 description LINK:DESCRIPTION:FORMAT:WILL:GO:HERE:TYPE-L3-BACKBONE
 encapsulation dot1Q 3201
 ip address 64.16.255.8 255.255.255.254
 ip mtu 8000
 ip router isis TELNYX-BACKBONE
 bfd template BFD-750X-3X-INT-STD
 clns mtu 1500
 isis network point-to-point 
 isis metric 20
!
interface Port-channel1.3202
 description LINK:DESCRIPTION:FORMAT:WILL:GO:HERE:TYPE-L3-BACKBONE
 encapsulation dot1Q 3202
 ip address 64.16.255.10 255.255.255.254
 ip mtu 8000
 ip router isis TELNYX-BACKBONE
 bfd template BFD-750X-3X-INT-STD
 clns mtu 1500
 isis network point-to-point 
 isis metric 10000
!
interface Port-channel1.3204
 description LINK:DESCRIPTION:FORMAT:WILL:GO:HERE:TYPE-L3-BACKBONE
 encapsulation dot1Q 3204
 ip address 64.16.255.14 255.255.255.254
 ip mtu 8000
 ip router isis TELNYX-BACKBONE
 bfd template BFD-750X-3X-INT-STD
 clns mtu 1500
 isis network point-to-point 
 isis metric 10010
!
interface Port-channel1.3262
 description L3_IPSEC
 encapsulation dot1Q 3262
 ip address 64.16.253.162 255.255.255.248
 standby 1 ip 64.16.253.161
 standby 1 priority 120
 standby 1 preempt
 standby 1 name IPSEC-VPN
 standby 1 track 201 decrement 10
 bfd template BFD-750X-3X-INT-STD
!
interface Tunnel101
 description TLNX-YYZ-TOR1-PE01
 vrf forwarding TELNYX
 ip address 172.20.0.137 255.255.255.252
 ip mtu 8000
 ip nat inside
 keepalive 3 3
 tunnel source Port-channel1.101
 tunnel destination 10.114.226.195
 tunnel vrf TELNYX
 redundancy rii 101
 redundancy asymmetric-routing enable
 redundancy group 1 decrement 50
!
interface Tunnel107
 description TLNX-YYZ-TOR1-PE04_dp0bond0_VIA_1G_DL_Po1.101_0130-1723-A
 vrf forwarding TELNYX
 ip address 172.20.2.33 255.255.255.252
 ip mtu 8000
 ip nat inside
 keepalive 3 3
 tunnel source Port-channel1.101
 tunnel destination 10.114.226.223
 tunnel vrf TELNYX
 redundancy rii 107
 redundancy asymmetric-routing enable
!
interface Tunnel207
 description TLNX-YYZ-TOR1-PE05_dp0bond0_VIA_1G_DL_Po1.101_0130-1723-A
 vrf forwarding TELNYX
 ip address 172.20.2.37 255.255.255.252
 ip mtu 8000
 ip nat inside
 keepalive 3 3
 tunnel source Port-channel1.101
 tunnel destination 10.114.226.243
 tunnel vrf TELNYX
 redundancy rii 207
 redundancy asymmetric-routing enable
!
interface GigabitEthernet0/0/0
 description LINK:DESCRIPTION:FORMAT:WILL:GO:HERE:FLAG-NO-IP-ADDRESS
 no ip address
 shutdown
 negotiation auto
!
interface GigabitEthernet0/0/1
 description LINK:DESCRIPTION:FORMAT:WILL:GO:HERE:FLAG-NO-IP-ADDRESS
 no ip address
 shutdown
 negotiation auto
!
interface GigabitEthernet0/0/2
 description LINK:DESCRIPTION:FORMAT:WILL:GO:HERE:FLAG-NO-IP-ADDRESS
 no ip address
 shutdown
 negotiation auto
!
interface GigabitEthernet0/0/3
 description LINK:DESCRIPTION:FORMAT:WILL:GO:HERE:FLAG-NO-IP-ADDRESS
 no ip address
 shutdown
 negotiation auto
!
interface GigabitEthernet0/0/4
 description LINK:DESCRIPTION:FORMAT:WILL:GO:HERE:FLAG-NO-IP-ADDRESS
 no ip address
 shutdown
 negotiation auto
!
interface GigabitEthernet0/0/5
 description LINK:DESCRIPTION:FORMAT:WILL:GO:HERE:FLAG-NO-IP-ADDRESS
 no ip address
 shutdown
 negotiation auto
!
interface TenGigabitEthernet0/1/0
 description L1_LOCAL_TLNX-YYZ-TR1-AG01_XE0/0/0
 mtu 9000
 no ip address
 carrier-delay msec 50
 lacp rate fast
 channel-group 1 mode active
!
interface TenGigabitEthernet0/2/0
 description L1_LOCAL_TLNX-YYZ-TR1-AG01_XE0/0/1
 mtu 9000
 no ip address
 carrier-delay msec 50
 lacp rate fast
 channel-group 1 mode active
!
interface TenGigabitEthernet0/3/0
 description L1_LOCAL_TLNX-YYZ-TR1-AG01_XE1/0/0
 mtu 9000
 no ip address
 carrier-delay msec 50
 lacp rate fast
 channel-group 1 mode active
!
interface GigabitEthernet0
 description L1_LOCAL_TLNX-YYZ-TR1-OPG01_PORT03
 vrf forwarding Mgmt-intf
 ip address 172.21.7.3 255.255.255.0
 negotiation auto
!
interface vasileft1
 description L3:GLOBAL:
 bandwidth 1000000
 ip address 64.16.255.28 255.255.255.254
 ip router isis TELNYX-BACKBONE
 no keepalive
!
interface vasiright1
 description L3:VRF:TELNYX
 bandwidth 1000000
 vrf forwarding TELNYX
 ip address 64.16.255.29 255.255.255.254
 ip nat outside
 no keepalive
 redundancy rii 1
 redundancy asymmetric-routing enable
 redundancy group 1 decrement 50
!
router isis TELNYX-BACKBONE
 net 49.0000.0001.0006.0003.00
 is-type level-2-only
 ispf level-1-2 60
 metric-style wide
 fast-flood 10
 set-overload-bit on-startup wait-for-bgp suppress external
 spf-interval 5 1 20
 prc-interval 5 1 20
 lsp-gen-interval 5 1 20
 log-adjacency-changes
 fast-reroute per-prefix level-2 all
 fast-reroute remote-lfa level-2 mpls-ldp
 microloop avoidance rib-update-delay 1
 passive-interface Loopback0
 passive-interface Port-channel1.3262
 bfd all-interfaces
 !
 address-family ipv6
  bfd all-interfaces
  multi-topology
 exit-address-family
 mpls ldp autoconfig
!
router bgp 63440
 bgp router-id 64.16.255.223
 bgp cluster-id 0.0.15.1
 bgp log-neighbor-changes
 bgp deterministic-med
 bgp graceful-restart restart-time 120
 bgp graceful-restart stalepath-time 360
 bgp graceful-restart
 no bgp default ipv4-unicast
 neighbor PG.V4.CORE.BBONE peer-group
 neighbor PG.V4.CORE.BBONE remote-as 63440
 neighbor PG.V4.CORE.BBONE update-source Loopback0
 neighbor PG.V4.CORE.BBONE timers 5 15
 neighbor PG.V4.CORE.BBONE ha-mode graceful-restart
 neighbor PG.V4.CORE.TO.EDGE peer-group
 neighbor PG.V4.CORE.TO.EDGE remote-as 63440
 neighbor PG.V4.CORE.TO.EDGE update-source Loopback0
 neighbor PG.V4.CORE.TO.EDGE timers 5 15
 neighbor PG.V4.CORE.TO.EDGE ha-mode graceful-restart
 neighbor PG.V4.CORE.LOCAL peer-group
 neighbor PG.V4.CORE.LOCAL remote-as 63440
 neighbor PG.V4.CORE.LOCAL update-source Loopback0
 neighbor PG.V4.CORE.LOCAL timers 5 15
 neighbor PG.V4.CORE.LOCAL ha-mode graceful-restart
 neighbor PG.V4.VASI.LEFT.TO.RIGHT peer-group
 neighbor PG.V4.VASI.LEFT.TO.RIGHT remote-as 63440
 neighbor PG.V4.VASI.LEFT.TO.RIGHT timers 5 15
 neighbor 64.16.255.29 peer-group PG.V4.VASI.LEFT.TO.RIGHT
 neighbor 64.16.255.29 description PEER:VRF-TELNYX-VASIRIGHT
 neighbor 64.16.255.220 peer-group PG.V4.CORE.TO.EDGE
 neighbor 64.16.255.220 description TLNX-YYZ-TR1-ER02
 neighbor 64.16.255.221 peer-group PG.V4.CORE.TO.EDGE
 neighbor 64.16.255.221 description TLNX-YYZ-TR1-ER01
 neighbor 64.16.255.222 peer-group PG.V4.CORE.LOCAL
 neighbor 64.16.255.222 description PEER:TLNX-YYZ-TR1-CR02
 neighbor 64.16.255.243 peer-group PG.V4.CORE.BBONE
 neighbor 64.16.255.243 description PEER:TLNX-ORD-CH1-CR01
 neighbor 64.16.255.247 peer-group PG.V4.CORE.BBONE
 neighbor 64.16.255.247 description PEER_TLNX-YVR-VAN2-CR01
 !
 address-family ipv4
  bgp additional-paths install
  bgp nexthop route-map RM-BGP-VALID-NEXTHOP-TRACKING
  bgp nexthop trigger delay 1
  network 64.16.224.0 mask 255.255.224.0
  network 192.76.120.0
  neighbor PG.V4.CORE.BBONE send-community
  neighbor PG.V4.CORE.BBONE route-reflector-client
  neighbor PG.V4.CORE.TO.EDGE send-community
  neighbor PG.V4.CORE.TO.EDGE route-reflector-client
  neighbor PG.V4.CORE.TO.EDGE route-map RM.NET4.EDGE.TO.CORE in
  neighbor PG.V4.CORE.TO.EDGE route-map RM.NET4.CORE.TO.EDGE out
  neighbor PG.V4.CORE.LOCAL send-community
  neighbor PG.V4.CORE.LOCAL next-hop-self
  neighbor PG.V4.VASI.LEFT.TO.RIGHT send-community
  neighbor PG.V4.VASI.LEFT.TO.RIGHT route-reflector-client
  neighbor PG.V4.VASI.LEFT.TO.RIGHT next-hop-self
  neighbor PG.V4.VASI.LEFT.TO.RIGHT default-originate route-map RM.NET4.TRACK.CONDITION
  neighbor PG.V4.VASI.LEFT.TO.RIGHT route-map RM.NET4.DEFAULT.ONLY out
  neighbor 64.16.255.29 activate
  neighbor 64.16.255.220 activate
  neighbor 64.16.255.221 activate
  neighbor 64.16.255.222 activate
  neighbor 64.16.255.243 activate
  neighbor 64.16.255.247 activate
 exit-address-family
 !
 address-family vpnv4
  bgp additional-paths install
  bgp nexthop route-map RM-BGP-VALID-NEXTHOP-TRACKING
  neighbor PG.V4.CORE.BBONE send-community both
  neighbor PG.V4.CORE.BBONE route-reflector-client
  neighbor PG.V4.CORE.TO.EDGE send-community both
  neighbor PG.V4.CORE.TO.EDGE route-reflector-client
  neighbor PG.V4.CORE.LOCAL send-community both
  neighbor PG.V4.CORE.LOCAL next-hop-self
  neighbor 64.16.255.220 activate
  neighbor 64.16.255.221 activate
  neighbor 64.16.255.222 activate
  neighbor 64.16.255.243 activate
  neighbor 64.16.255.247 activate
 exit-address-family
 !
 address-family ipv4 vrf TELNYX
  import path selection all
  bgp router-id 172.18.0.36
  network 172.18.0.36 mask 255.255.255.255
  network 172.18.16.0 mask 255.255.255.248
  network 172.20.2.32 mask 255.255.255.252
  network 172.20.2.36 mask 255.255.255.252
  network 192.76.120.198 mask 255.255.255.255
  neighbor 64.16.255.28 remote-as 63440
  neighbor 64.16.255.28 description PEER:VRF-TELNYX-VASILEFT  
  neighbor 64.16.255.28 timers 5 15
  neighbor 64.16.255.28 ha-mode graceful-restart
  neighbor 64.16.255.28 activate
  neighbor 64.16.255.28 send-community
  neighbor 64.16.255.28 route-reflector-client
  neighbor 64.16.255.28 next-hop-self
  neighbor 64.16.255.28 route-map RM.NET4.TELNYX.VASI.RIGHT.TO.LEFT.OUT out
  neighbor 172.20.0.138 remote-as 65307
  neighbor 172.20.0.138 description PEER:TLNX-YYZ-TOR1-PE01
  neighbor 172.20.0.138 shutdown
  neighbor 172.20.0.138 ebgp-multihop 255
  neighbor 172.20.0.138 timers 3 12
  neighbor 172.20.0.138 activate
  neighbor 172.20.0.138 advertisement-interval 1
  neighbor 172.20.0.138 route-map RM.NET4.INTERNAL.INGRESS.POLICY in
  neighbor 172.20.0.138 route-map RM.NET4.DEFAULT.ONLY out
  neighbor 172.20.2.34 remote-as 65307
  neighbor 172.20.2.34 description PEER:TLNX-YYZ-TOR1-PE04 VIA Tunnel107
  neighbor 172.20.2.34 ebgp-multihop 255
  neighbor 172.20.2.34 update-source Tunnel107
  neighbor 172.20.2.34 timers 30 90
  neighbor 172.20.2.34 activate
  neighbor 172.20.2.34 send-community both
  neighbor 172.20.2.34 advertisement-interval 1
  neighbor 172.20.2.34 route-map RM.NET4.INTERNAL.INGRESS.POLICY in
  neighbor 172.20.2.34 route-map RM.NET4.DEFAULT.ONLY out
  neighbor 172.20.2.38 remote-as 65307
  neighbor 172.20.2.38 description PEER:TLNX-YYZ-TOR1-PE05 VIA Tunnel207
  neighbor 172.20.2.38 ebgp-multihop 255
  neighbor 172.20.2.38 update-source Tunnel207
  neighbor 172.20.2.38 timers 30 90
  neighbor 172.20.2.38 activate
  neighbor 172.20.2.38 send-community both
  neighbor 172.20.2.38 advertisement-interval 1
  neighbor 172.20.2.38 route-map RM.NET4.INTERNAL.INGRESS.POLICY in
  neighbor 172.20.2.38 route-map RM.NET4.DEFAULT.ONLY out
 exit-address-family
!
ip nat pool PAT.VRF.TELNYX.01 192.76.120.198 192.76.120.198 prefix-length 27
ip nat inside source route-map RM.PAT.TELNYX.01 pool PAT.VRF.TELNYX.01 redundancy 1 mapping-id 1001 vrf TELNYX overload
ip forward-protocol nd
!
ip bgp-community new-format
ip community-list expanded CL.LOCAL.PREF.100 permit 101:100
ip community-list expanded CL.LOCAL.PREF.150 permit 101:150
ip community-list expanded CL.LOCAL.PREF.200 permit 101:200
ip community-list expanded CL.LOCAL.PREF.250 permit 101:250
ip community-list expanded CL.LOCAL.PREF.300 permit 101:300
ip community-list expanded CL.LOCAL.PREF.350 permit 101:350
ip community-list expanded CL.LOCAL.PREF.400 permit 101:400
ip community-list expanded CL.LOCAL.PREF.450 permit 101:450
ip community-list expanded CL.LOCAL.PREF.500 permit 101:500
ip community-list expanded CL.LOCAL.PREF.550 permit 101:550
ip community-list expanded CL.LOCAL.PREF.600 permit 101:600
ip community-list expanded CL.LOCAL.PREF.650 permit 101:650
ip community-list expanded CL.LOCAL.PREF.700 permit 101:700
ip community-list expanded CL.LOCAL.PREF.750 permit 101:750
ip community-list expanded CL.LOCAL.PREF.800 permit 101:800
ip community-list expanded CL.LOCAL.PREF.850 permit 101:850
ip community-list expanded CL.LOCAL.PREF.900 permit 101:900
ip community-list expanded CL.LOCAL.PREF.950 permit 101:950
ip ftp source-interface Loopback1001
ip ftp username netftp
ip ftp password 7 1500123A05181F7016771A663E5F2F2909
no ip http server
no ip http secure-server
ip tftp source-interface GigabitEthernet0
ip route vrf TELNYX 192.76.120.198 255.255.255.255 Null0 name NET4.VRF-TELNYX.PAT track 201
ip route 64.16.224.0 255.255.224.0 Null0 name NET4.NULL.MAJOR.BLOCK track 201
ip route 192.76.120.0 255.255.255.0 Null0 name NET4.NULL.MAJOR.BLOCK track 201
ip route vrf TELNYX 10.114.226.195 255.255.255.255 10.254.1.253 name TLNX-YYZ-TOR1-PE01 track 201
ip route 100.64.128.1 255.255.255.255 Null0 name NET4.NULL.TRACKING.ROUTE track 201
ip route vrf Mgmt-intf 0.0.0.0 0.0.0.0 172.21.7.1 name DEFAULT.ROUTE.OOB
ip route vrf TELNYX 10.114.226.223 255.255.255.255 10.254.1.253 name L3:SOFTLAYER:DL:TLNX-YYZ-TOR1-PE04
ip route vrf TELNYX 10.114.226.243 255.255.255.255 10.254.1.253 name L3:SOFTLAYER:DL:TLNX-YYZ-TOR1-PE05
ip ssh time-out 30
ip ssh logging events
ip ssh version 2
ip ssh pubkey-chain
  username network
   key-hash ssh-rsa E5E1A29B154ABF6FED13C97B728E0CDA network@telnyx.com
  username tena
   key-hash ssh-rsa E294BBC9D0B923163C0B6E6737CBB2D7 rancid@ch1-netmgmt02.ipa.corp.telnyx.com
  username tena_prod_pipeline
   key-hash ssh-rsa 92A01AA44A20393B16628D52D7EA77B7 rancid@ch1-netmgmt02.ipa.corp.telnyx.com
ip scp server enable
!
ip access-list standard MGMT
 permit 10.0.0.0 0.255.255.255
 permit 172.16.0.0 0.15.255.255
 permit 64.16.224.0 0.0.31.255
!
ip access-list extended ACL.NET4.RFC1918
 permit ip 10.0.0.0 0.255.255.255 any
 permit ip 172.16.0.0 0.15.255.255 any
 permit ip 192.168.0.0 0.0.255.255 any
ip access-list extended ACL.PAT.TELNYX.01
 deny   tcp any any eq bgp
 permit ip 10.0.0.0 0.255.255.255 any
 permit ip 172.16.0.0 0.15.255.255 any
!
!
ip prefix-list PL-BGP-NO-19-AS-NEXTHOP seq 5 permit 64.16.224.0/19 le 24
!
ip prefix-list PL.NET4.DEFAULT.ROUTE seq 5 permit 0.0.0.0/0
!
ip prefix-list PL.NET4.DENY.ALL seq 5 deny 0.0.0.0/0 le 32
!
ip prefix-list PL.NET4.PERMIT.ALL seq 5 permit 0.0.0.0/0 le 32
!
ip prefix-list PL.NET4.RFC1918 seq 5 permit 10.0.0.0/8
ip prefix-list PL.NET4.RFC1918 seq 10 permit 172.16.0.0/12
ip prefix-list PL.NET4.RFC1918 seq 15 permit 192.168.0.0/16
!
ip prefix-list PL.NET4.TELNYX.PUBLIC.SUMMARY seq 5 permit 64.16.224.0/19
ip prefix-list PL.NET4.TELNYX.PUBLIC.SUMMARY seq 10 permit 192.76.120.0/24
!
ip prefix-list PL.NET4.TELNYX.SPECIFIC seq 5 permit 64.16.224.0/19 le 32
ip prefix-list PL.NET4.TELNYX.SPECIFIC seq 10 permit 192.76.120.0/24 le 32
!
ip prefix-list PL.NET4.TRACK.CONDITION seq 10 permit 100.64.128.1/32
ip sla responder
ip sla 1019
 udp-jitter 64.16.254.199 5060
 request-data-size 215
 tag SLA:JITTER:VLAN1019:TLNX-ORD-CH1-CR
 threshold 200
 timeout 300
 frequency 1
 history hours-of-statistics-kept 12
ip sla schedule 1019 life forever start-time now
ip sla 1021
 udp-jitter 64.16.254.203 5060
 request-data-size 215
 tag SLA:JITTER:VLAN1021:TLNX-YVR-VAN2-C
 threshold 200
 timeout 300
 frequency 1
 history hours-of-statistics-kept 12
ip sla schedule 1021 life forever start-time now
ip sla reaction-configuration 1019 react packetLossSD threshold-value 2 1 threshold-type immediate action-type trapAndTrigger
ip sla reaction-configuration 1019 react packetLossDS threshold-value 2 1 threshold-type immediate action-type trapAndTrigger
ip sla reaction-configuration 1019 react timeout threshold-type immediate action-type trapAndTrigger
ip sla reaction-configuration 1019 react connectionLoss threshold-type immediate action-type trapAndTrigger
ip sla reaction-configuration 1021 react packetLossSD threshold-value 2 1 threshold-type immediate action-type trapAndTrigger
ip sla reaction-configuration 1021 react packetLossDS threshold-value 2 1 threshold-type immediate action-type trapAndTrigger
ip sla reaction-configuration 1021 react timeout threshold-type immediate action-type trapAndTrigger
ip sla reaction-configuration 1021 react connectionLoss threshold-type immediate action-type trapAndTrigger
ip sla logging traps
ip sla enable reaction-alerts
logging source-interface Loopback1001 vrf TELNYX
logging host 10.1.129.5 vrf TELNYX
logging host 10.7.0.125 vrf TELNYX
logging host 10.7.0.216 vrf TELNYX
!
route-map RM.NET4.DEFAULT.ONLY permit 10
 match ip address prefix-list PL.NET4.DEFAULT.ROUTE
!
route-map RM.NET4.PERMIT.ALL permit 10
 match ip address prefix-list PL.NET4.PERMIT.ALL
!
route-map RM.NET4.DENY.ALL permit 10
 match ip address prefix-list PL.NET4.DENY.ALL
!
route-map RM.NET4.RFC1918 permit 10
 match ip address prefix-list PL.NET4.RFC1918
!
route-map RM.NET4.INTERNAL.INGRESS.POLICY permit 10
 match community CL.LOCAL.PREF.100
 set local-preference 100
!
route-map RM.NET4.INTERNAL.INGRESS.POLICY permit 20
 match community CL.LOCAL.PREF.150
 set local-preference 150
!
route-map RM.NET4.INTERNAL.INGRESS.POLICY permit 30
 match community CL.LOCAL.PREF.200
 set local-preference 200
!
route-map RM.NET4.INTERNAL.INGRESS.POLICY permit 40
 match community CL.LOCAL.PREF.250
 set local-preference 250
!
route-map RM.NET4.INTERNAL.INGRESS.POLICY permit 50
 match community CL.LOCAL.PREF.300
 set local-preference 300
!
route-map RM.NET4.INTERNAL.INGRESS.POLICY permit 60
 match community CL.LOCAL.PREF.350
 set local-preference 350
!
route-map RM.NET4.INTERNAL.INGRESS.POLICY permit 70
 match community CL.LOCAL.PREF.400
 set local-preference 400
!
route-map RM.NET4.INTERNAL.INGRESS.POLICY permit 80
 match community CL.LOCAL.PREF.450
 set local-preference 450
!
route-map RM.NET4.INTERNAL.INGRESS.POLICY permit 90
 match community CL.LOCAL.PREF.500
 set local-preference 500
!
route-map RM.NET4.INTERNAL.INGRESS.POLICY permit 100
 match community CL.LOCAL.PREF.550
 set local-preference 550
!
route-map RM.NET4.INTERNAL.INGRESS.POLICY permit 110
 match community CL.LOCAL.PREF.600
 set local-preference 600
!
route-map RM.NET4.INTERNAL.INGRESS.POLICY permit 120
 match community CL.LOCAL.PREF.650
 set local-preference 650
!
route-map RM.NET4.INTERNAL.INGRESS.POLICY permit 130
 match community CL.LOCAL.PREF.700
 set local-preference 700
!
route-map RM.NET4.INTERNAL.INGRESS.POLICY permit 140
 match community CL.LOCAL.PREF.750
 set local-preference 750
!
route-map RM.NET4.INTERNAL.INGRESS.POLICY permit 150
 match community CL.LOCAL.PREF.800
 set local-preference 800
!
route-map RM.NET4.INTERNAL.INGRESS.POLICY permit 160
 match community CL.LOCAL.PREF.850
 set local-preference 850
!
route-map RM.NET4.INTERNAL.INGRESS.POLICY permit 170
 match community CL.LOCAL.PREF.900
 set local-preference 900
!
route-map RM.NET4.INTERNAL.INGRESS.POLICY permit 180
 match community CL.LOCAL.PREF.950
 set local-preference 950
!
route-map RM.NET4.INTERNAL.INGRESS.POLICY permit 65535
!
route-map RM.NET4.TELNYX.PUBLIC.SUMMARY permit 10
 match ip address prefix-list PL.NET4.TELNYX.PUBLIC.SUMMARY
!
route-map RM-BGP-VALID-NEXTHOP-TRACKING deny 10
 match ip address prefix-list PL-BGP-NO-19-AS-NEXTHOP
 match source-protocol static
!
route-map RM-BGP-VALID-NEXTHOP-TRACKING permit 20
!
route-map RM.NET4.TRACK.CONDITION permit 10
 match ip address prefix-list PL.NET4.TRACK.CONDITION
!
route-map RM.NET4.TELNYX.VASI.RIGHT.TO.LEFT.OUT permit 10
 match ip address prefix-list PL.NET4.TELNYX.SPECIFIC
!
route-map RM.NET4.REDIST.STATIC.SET.COMMUNITY permit 10
 match tag 102
 set origin igp
 set community 102:1
!
route-map RM.NET4.REDIST.STATIC.SET.COMMUNITY deny 65535
 match ip address prefix-list PL.NET4.PERMIT.ALL
!
route-map RM.PAT.TELNYX.01 permit 10
 match ip address ACL.PAT.TELNYX.01
!
route-map RM.NET4.CORE.TO.EDGE permit 10
 match ip address prefix-list PL.NET4.TELNYX.SPECIFIC
!
route-map RM.NET4.EDGE.TO.CORE deny 10
 match ip address prefix-list PL.NET4.TELNYX.SPECIFIC
!
route-map RM.NET4.EDGE.TO.CORE permit 20
 match ip address prefix-list PL.NET4.PERMIT.ALL
!
route-map RM_NET4_DENY_ALL deny 10
!
route-map RM_NET6_DENY_ALL deny 10
!
snmp-server community Telnyx1 RO
snmp-server location YYZ-TR1
snmp-server contact networking@telnyx.com
snmp-server enable traps ipsla
snmp-server enable traps syslog
snmp ifmib ifindex persist
mpls ldp router-id Loopback0
!
!
!
!
control-plane
!
 !
 !
 !
 !
!
!
!
!
alias exec arp show ip arp
alias exec bgpsum show ip bgp summary
alias exec cdp show cdp neighbor
alias exec desc show inter desc | ex admin down
alias exec down show ip int br | include down
alias exec intbr show ip int br
alias exec ip show ip int br | ex unass
alias exec ldp show mpls ldp neighbor | in Peer LDP
alias exec lfib show mpls forwarding-table
alias exec lldp show lldp neighbor
alias exec mem show platform software status control-processor brief
alias exec red sh redundancy application group
alias exec ro sh ip ro
alias exec scon show running-config line
alias exec sla show ip sla statistics | in IPSLA operation id:|Number of
alias exec tcam show platform hardware qfp active tcam resource-manager usage
alias exec up show ip int br | include up
alias exec vpn sh crypto ipsec sa | in current_peer|local ident|send error|caps
alias exec vpnc show crypto session | in Username!
!
line con 0
 exec-timeout 15 0
 privilege level 15
 logging synchronous
 login authentication CONSOLE
 stopbits 1
line aux 0
 stopbits 1
line vty 0 4
 access-class MGMT in vrf-also
 exec-timeout 60 0
 privilege level 15
 logging synchronous
 transport input ssh
line vty 5 97
 access-class MGMT in vrf-also
 exec-timeout 60 0
 privilege level 15
 logging synchronous
 length 0
 transport preferred ssh
 transport input ssh
!
ntp logging
ntp peer 64.16.232.10
ntp peer 64.16.238.10
ntp server 64.16.253.165
ntp server 64.16.253.166
!
event manager applet NAT-RG-TRACKER-UP
 event syslog pattern "Init to"
 action 201 track set 201 state up
event manager applet NAT-RG-TRACKER-DOWN
 event syslog pattern "to Init"
 action 201 track set 201 state down
!
end
"""

y = x.split('\n')
print(y)