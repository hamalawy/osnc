import re

mystr = 'abc123def456ghi'
user_input1 = r'(\d+).+?(\d+)'
user_input2 = r'\2\1'
match = re.search(user_input1, mystr)
result = match.expand(user_input2)


import pdb; pdb.set_trace()

type(match)
print(match)
type(result)
print(result)